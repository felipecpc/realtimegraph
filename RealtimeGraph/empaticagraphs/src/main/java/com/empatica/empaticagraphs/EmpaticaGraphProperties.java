package com.empatica.empaticagraphs;

import android.graphics.Color;

import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.Random;

/**
 * Created by felipe on 17/07/16.
 */
public class EmpaticaGraphProperties {

    private int backgroundColor = Color.WHITE;
    private int datasetColor[] = null;
    private String dataSetLabel[] = null;
    private int datasetCount=1;

    public String getDataSetLabel(int index) {
        if(dataSetLabel==null) return "Data set " + index;
        return dataSetLabel[index];
    }

    public void setDataSetLabel(String[] dataSetLabel) {
        this.dataSetLabel = dataSetLabel;
    }

    public int getDatasetCount() {
        return datasetCount;
    }

    public void setDatasetCount(int datasetCount) {
        this.datasetCount = datasetCount;
    }

    public int getBackgroundColor() {

        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getDatasetColor(int index) {
        if(datasetColor==null){
            if(datasetCount==1) return ColorTemplate.getHoloBlue();
            else{
                return Color.rgb(randInt(),randInt(),randInt());
            }
        }else return datasetColor[index];
    }

    public void setDatasetColor(int[] datasetColor) {
        this.datasetColor = datasetColor;
    }


    private int randInt() {

        Random rand = new Random();
        return rand.nextInt(256);

    }
}
