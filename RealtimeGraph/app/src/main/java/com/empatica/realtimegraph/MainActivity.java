package com.empatica.realtimegraph;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.empatica.empaticagraphs.EmpaticaGraph;
import com.empatica.empaticagraphs.EmpaticaGraphProperties;
import com.empatica.realtimegraph.ble.BLEConnection;
import com.empatica.realtimegraph.ble.BluetoothLeService;
import com.empatica.realtimegraph.ble.SampleGattAttributes;
import com.github.mikephil.charting.charts.LineChart;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements EmpaDataInterface{




    BLEConnection bleConnection;

    private EmpaticaGraph mEmpaticaGraph;
    private EmpaticaGraph mEmpaticaGraph1;
    private EmpaticaGraph mEmpaticaGraph2;
    private LineChart mChart;
    private LineChart mChart1;
    private LineChart mChart2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //----------------------------
        // GRAPH 1
        //----------------------------
        mChart = (LineChart) findViewById(R.id.mychart);
        EmpaticaGraphProperties lProperties = new EmpaticaGraphProperties();
        lProperties.setDatasetCount(3);
        String lLabels[]={"Gyroscope X","Y","Z"};
        lProperties.setDataSetLabel(lLabels);
        //Set graph with default properties
        mEmpaticaGraph = new EmpaticaGraph(mChart, lProperties);

        //----------------------------
        // GRAPH 2
        //----------------------------
        mChart1 = (LineChart) findViewById(R.id.mychart1);
        //Define graph properties
        EmpaticaGraphProperties lProperties1 = new EmpaticaGraphProperties();
        lProperties1.setBackgroundColor(Color.rgb(240, 240, 30));
        lProperties1.setDatasetCount(3);
        String lLabels1[]={"Magnetometer X","Y","Z"};
        lProperties1.setDataSetLabel(lLabels1);
        //Set graph
        mEmpaticaGraph1 = new EmpaticaGraph(mChart1,lProperties1);

        //----------------------------
        // GRAPH 3
        //----------------------------
        mChart2 = (LineChart) findViewById(R.id.mychart2);
        //Define graph properties
        EmpaticaGraphProperties lProperties2 = new EmpaticaGraphProperties();
        lProperties2.setBackgroundColor(Color.rgb(89, 199, 250));
        lProperties2.setDatasetCount(3);
        String lLabels2[]={"Acceleremeter X","Y","Z"};
        lProperties2.setDataSetLabel(lLabels2);
        //Set graph
        mEmpaticaGraph2 = new EmpaticaGraph(mChart2, lProperties2);

        bleConnection = new BLEConnection(getApplication(),MainActivity.this);



    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onAccelerationChanged(long x, long y, long z, double timestamp) {
        long values[] = new long[3];
        values[0] = x;
        values[1] = y;
        values[2]=z;

        mEmpaticaGraph2.onDataChanged(values, System.currentTimeMillis());
    }

    @Override
    public void onMagnetometerChanged(long x, long y, long z, double timestamp) {
        long values[] = new long[3];
        values[0] = x;
        values[1] = y;
        values[2]=z;

        mEmpaticaGraph1.onDataChanged(values, System.currentTimeMillis());
    }

    @Override
    public void onGyroscopeChanged(long x, long y, long z, double timestamp) {
        long values[] = new long[3];
        values[0] = x;
        values[1] = y;
        values[2]=z;

        mEmpaticaGraph.onDataChanged(values, System.currentTimeMillis());
    }




}
