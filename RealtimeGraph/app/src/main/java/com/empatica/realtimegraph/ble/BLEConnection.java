package com.empatica.realtimegraph.ble;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

import com.empatica.realtimegraph.EmpaDataInterface;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by felipe on 20/07/2016.
 */
public class BLEConnection {

    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 30000;

    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;

    private final String  TAG = "BLEConnection";
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    public static Handler messageHandler;

    private String mDeviceAddress;
    private EmpaDataInterface iEmpaDataInterface;
    private Context mCtx;

    public BLEConnection(Context ctx, EmpaDataInterface empaInterface){

        iEmpaDataInterface=empaInterface;

        mCtx = ctx;

        mHandler = new Handler();

        if (!mCtx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(mCtx, "Device does not support BLE, sorry :(", Toast.LENGTH_SHORT).show();
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) mCtx.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(mCtx, "Device does not support BLE, sorry :(", Toast.LENGTH_SHORT).show();

        }


        if (!mBluetoothAdapter.isEnabled()){
            mBluetoothAdapter.enable();
        }

        scanLeDevice(true);
    }

    public boolean connect(){
        messageHandler = new MessageHandler();
        Intent gattServiceIntent = new Intent(mCtx, BluetoothLeService.class);
        gattServiceIntent.putExtra("MESSENGER", new Messenger(messageHandler));
        mCtx.bindService(gattServiceIntent, mServiceConnection, mCtx.BIND_AUTO_CREATE);

        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
            return false;
        }

        return true;
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                //finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };



    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        for (BluetoothGattService gattService : gattServices) {

            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();

            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                if ((gattCharacteristic.getProperties() | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                    if(BluetoothLeService.UUID_WICED_NOPTIFICATION.equals(gattCharacteristic.getUuid())) {
                        mNotifyCharacteristic = gattCharacteristic;
                        if(mBluetoothLeService.setCharacteristicNotification(
                                gattCharacteristic, true)) {
                            mBluetoothLeService.readCharacteristic(gattCharacteristic);
                        }
                    }
                }
            }
        }
    }

    public class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            Bundle d = message.getData();

            if(d.getString("command").equals(BluetoothLeService.ACTION_GATT_CONNECTED)){
                mConnected = true;
                Log.d(TAG, "Connected!!!");
            }else if(d.getString("command").equals(BluetoothLeService.ACTION_GATT_DISCONNECTED)){
                mConnected = false;
                Log.d(TAG, "Disconnected!!!");
                scanLeDevice(true);
            }else if(d.getString("command").equals(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED)){
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            }else {
                byte data[] = d.getByteArray("data");
                if(data!=null) {
                    if (data[0] == 0x0B) {
                        int x = data[1] * 256 + data[2];
                        int y = data[3] * 256 + data[4];
                        int z = data[5] * 256 + data[6];
                        iEmpaDataInterface.onAccelerationChanged(x, y, z, 0);
                        x = data[7] * 256 + data[8];
                        y = data[9] * 256 + data[10];
                        z = data[11] * 256 + data[12];
                        iEmpaDataInterface.onMagnetometerChanged(x, y, z, 0);
                        x = data[13] * 256 + data[14];
                        y = data[15] * 256 + data[16];
                        z = data[17] * 256 + data[18];
                        iEmpaDataInterface.onGyroscopeChanged(x, y, z, 0);
                    }
                }
            }
        }
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

                    if(device.getName()!=null && device.getName().equals("WICED Sense Kit")){
                        mDeviceAddress=device.getAddress();
                        connect();
                        scanLeDevice(false);
                    }

                }
            };
}
