package com.empatica.realtimegraph;

/**
 * Created by felipe on 21/07/16.
 */
public interface EmpaDataInterface {
    void onAccelerationChanged(long x, long y, long z, double timestamp);
    void onMagnetometerChanged(long x, long y, long z, double timestamp);
    void onGyroscopeChanged(long x, long y, long z, double timestamp);
}
