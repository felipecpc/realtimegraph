package com.empatica.empaticalogger;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

public class EmpaticaLogger {

    private final String TAG = "EmpaticaLogger";

    public static final int DEBUG =   3;
    public static final int INFO =    2;
    public static final int WARNING = 1;
    public static final int ERROR =   0;


    private String mLevelLabel[] = {"[ERROR] ","[WARNING] ","[INFO] ","[DEBUG] "};

    private boolean mLogEntriesEnabled = true;
    private boolean mConsoleEnabled = true;
    private int mLogEntriesLevel = 3;
    private int mConsoleLevel = 3;

    private static EmpaticaLogger instance;

    private AsyncLoggingWorker loggingWorker;

    private EmpaticaLogger(Context context, boolean useHttpPost, boolean useSsl, boolean isUsingDataHub, String dataHubAddr, int dataHubPort,
                           String token, boolean logHostName) throws IOException {
        loggingWorker = new AsyncLoggingWorker(context, useSsl, useHttpPost, isUsingDataHub, token, dataHubAddr, dataHubPort, logHostName);
    }

    public static synchronized EmpaticaLogger createInstance(Context context, boolean useHttpPost, boolean useSsl, boolean isUsingDataHub,
                                                             String dataHubAddr, int dataHubPort, String token, boolean logHostName)
            throws IOException {
        if(instance != null) {
            instance.loggingWorker.close();
        }

        instance = new EmpaticaLogger(context, useHttpPost, useSsl, isUsingDataHub, dataHubAddr, dataHubPort, token, logHostName);
        return instance;
    }


    public static synchronized EmpaticaLogger createInstance(Context context, String token)
            throws IOException {
        if(instance != null) {
            instance.loggingWorker.close();
        }

        instance = createInstance(context, false, false, false, null, 0, token, false);
        return instance;
    }


    public static synchronized EmpaticaLogger getInstance() {
        if(instance != null) {
            return instance;
        } else {
            throw new IllegalArgumentException("Logger instance is not initialized. Call createInstance() first!");
        }
    }

    public void log(int level, String message) {
        if(mLogEntriesEnabled && level<=mLogEntriesLevel) {
            loggingWorker.addLineToQueue(mLevelLabel[level] + message);
        }

        if(mConsoleEnabled && level<=mConsoleLevel){
            switch(level){
                case ERROR:
                    Log.e(TAG,message);break;
                case INFO:
                    Log.i(TAG,message);break;
                case WARNING:
                    Log.w(TAG,message);break;
                case DEBUG:
                    Log.d(TAG,message);break;
            }
        }
    }


    public void configureLog(boolean logEntriesEnabled,int logEntriesLevel, boolean logConsoleEnabled,int logConsoleLevel){
        mLogEntriesEnabled = logEntriesEnabled;
        mConsoleEnabled = logConsoleEnabled;
        mLogEntriesLevel = logEntriesLevel;
        mConsoleLevel = logConsoleLevel;
    }

    public void enableLogEntries(boolean logEntriesEnabled){
        mLogEntriesEnabled = logEntriesEnabled;
    }

    public void enabledConsole(boolean logConsoleEnabled){
        mConsoleEnabled = logConsoleEnabled;
    }

    public void setLogEntriesLevel(int logEntriesLevel){
        mLogEntriesLevel = logEntriesLevel;
    }

    public void enabledConsole(int consoleLevel){
        mConsoleLevel = consoleLevel;
    }

}
